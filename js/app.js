var app = new Vue({

  el: "#root",
  data: {
  	showingaddModal: false,
  	showingeditModal: false,
  	showingdeleteModal: false,
  	errorMessage: "",
  	successMessage: "",
  	mahasiswa: [],
  	newMahasiswa: {nama: "", email: "", nim: ""},
  	clickedMahasiswa: {},
  },

  mounted: function () {
  	console.log("Vue.js is running...");
  	this.getAllMahasiswa();
  },

  methods: {
  	getAllMahasiswa: function () {
  		axios.get('http://localhost/uasvuejs/api/api.php?action=read')
  		.then(function (response) {
  			console.log(response);

  			if (response.data.error) {
  				app.errorMessage = response.data.message;
  			} else {
  				app.mahasiswa = response.data.mahasiswa;
  			}
  		})
  	},

  	addMahasiswa: function () {
  		var formData = app.toFormData(app.newMahasiswa);
  		axios.post('http://localhost/uasvuejs/api/api.php?action=create', formData)
  		.then(function (response) {
  			console.log(response);
  			app.newMahasiswa = {nama: "", email: "", nim: ""};

  			if (response.data.error) {
  				app.errorMessage = response.data.message;
  			} else {
  				app.successMessage = response.data.message;
  				app.getAllMahasiswa();
  			}
  		});
  	},

  	updateMahasiswa: function () {
  		var formData = app.toFormData(app.clickedMahasiswa);
  		axios.post('http://localhost/uasvuejs/api/api.php?action=update', formData)
  		.then(function (response) {
  			console.log(response);
  			app.clickedMahasiswa = {};

  			if (response.data.error) {
  				app.errorMessage = response.data.message;
  			} else {
  				app.successMessage = response.data.message;
  				app.getAllMahasiswa();
  			}
  		});
  	},

  	deleteMahasiswa: function () {
  		var formData = app.toFormData(app.clickedMahasiswa);
  		axios.post('http://localhost/uasvuejs/api/api.php?action=delete', formData)
  		.then(function (response) {
  			console.log(response);
  			app.clickedMahasiswa = {};

  			if (response.data.error) {
  				app.errorMessage = response.data.message;
  			} else {
  				app.successMessage = response.data.message;
  				app.getAllMahasiswa();
  			}
  		})
  	},

  	selectMahasiswa(mahasiswa) {
  		app.clickedMahasiswa = mahasiswa;
  	},

  	toFormData: function (obj) {
  		var form_data = new FormData();
  		for (var key in obj) {
  			form_data.append(key, obj[key]);
  		}
  		return form_data;
  	},

  	clearMessage: function (argument) {
  		app.errorMessage   = "";
  		app.successMessage = "";
  	},


  }
});